# fgcov - a basic coverage analysis example with CMake and Fortran #

As it says above. When run under Windows, MinGW's GCC toolchain puts `.gcda` and `.gcno` files in different directories than under `bash`. This makes no sense, but since I'm running GCC on Windows, I've come to expect this sort of behavior.
