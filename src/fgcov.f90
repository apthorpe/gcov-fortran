!> A program which should show test coverage
program fgcov
  use m_tangential, only: frequently, rarely
  implicit none
  integer :: i
  integer :: j
  integer, parameter :: nmany = 100
  integer, parameter :: nfew = 10
  continue

  do j = 1, nfew
    do i = 1, nmany
      call frequently(i, j)
    end do
    call rarely(i + j)
  end do

end program fgcov