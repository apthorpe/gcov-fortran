!> Example functions for coverage testing
module m_tangential
  implicit none
  private

  public :: frequently
  public :: rarely

contains

!> A subroutine called frequently
subroutine frequently(i, j)
  implicit none
  integer, intent(in) :: i
  integer, intent(in) :: j
  continue

  if (modulo(i * j, 13) == 2) then
    call rarely(i+j)
  end if

  return
end subroutine frequently

!> A subroutine called infrequently
subroutine rarely(j)
  use, intrinsic :: iso_fortran_env, only: stdout => OUTPUT_UNIT
  implicit none
  integer, intent(in) :: j
  continue

  if (modulo(j, 3) == 1) then
    write(unit=stdout, fmt='(A)') "Ping."
  end if

  return
end subroutine rarely

end module m_tangential